
## Un poco de mí

[Guillermo Santos Melgar](mailto:guillermo.santos-melgar@hp.com)

Geek y Software Engineer, con más de 15 años de experiencia en desarrollo de software, administración de servidores y gestión de repositorios (SCM). Actualmente realizando labores de Software Specialist en el grupo de A3 de HP SCDS.

- <span class="icofont-twitter"></span>@gsanmel
- <span class="icofont-bird-alt"></span>https://www.linkedin.com/in/neomorfeo
- <span class="icofont-email"></span>guillermo.santos-melgar@hp.com<br />
