# Step-0: Initial codebase and explanation

In this step we will cover the initial codebase to CI/CD.

## Structure
```sh
step-0
├── go.mod
├── go.sum
├── main.go
├── Makefile
└── README.md
```

`go.mod` and `go.sum` : The files  are required ones for dependencies and structure of go itself.

`Makefile`: File to control and integrate/plain the way of execution our flow

`main.go`: Main file to our program

`README.md`: This file

## How to start

Please check your GO installation first, after is done, please run `make dep` to download all dependencies and after is done, execute `make run`.

You will have a server running on [http://localhost:8080](http://localhost:8080). For documentation on this, please check  [Project swagger Docs](http://localhost:8080/swagger/index.html)

