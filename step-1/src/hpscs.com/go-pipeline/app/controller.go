//Package app for testing and have several functionts
package app

// ExampleHello say hello
func ExampleHello() string {
	return "hello"
	// Output: hello
}

// ExampleSalutations say hello and bye
func ExampleSalutations() string {
	return "hello, and \ngoodbye"
	// Output:
	// hello, and
	// goodbye
}

//HealthCheck returns the status of the server
func HealthCheck() string {
	return "Ok!"
	// Output: Ok!
}
